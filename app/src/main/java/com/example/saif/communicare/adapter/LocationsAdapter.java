package com.example.saif.communicare.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.activity.CampusDetails;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.model.MedicationModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class LocationsAdapter extends ArrayAdapter<LocationModel> {
    private ArrayList<LocationModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    String type;

    public LocationsAdapter(Context context, int resource, ArrayList<LocationModel> models, String type) {
        super(context, resource, models);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
        this.context = context;
        this.type = type;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.locations_item, null);
            holder.viewServices = convertView.findViewById(R.id.view);
            holder.title = convertView.findViewById(R.id.title);
            holder.address = convertView.findViewById(R.id.address);
            holder.timingTxt = convertView.findViewById(R.id.timingTxt);
            holder.timing = convertView.findViewById(R.id.timing);
            holder.phone = convertView.findViewById(R.id.phone);
            holder.phonePanel = convertView.findViewById(R.id.phonePanel);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.viewServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CampusDetails.class);
                intent.putExtra("type", type);
                intent.putExtra("position", position);
                context.startActivity(intent);
            }
        });

        if(type.equals("1")){
            if(position == 0){
                setAllData(holder, new LocationModel("West Campus","1102 Barclay Street San Antonio, Texas 78207",
                        "Monday to Friday","8:00am to 7:00pm","(210) 233-7000"));
            }
            else if(position == 1){
                setAllData(holder, new LocationModel("East Campus","3066 East Commerce San Antonio, Texas 78220",
                        "Monday to Friday","8:00am to 7:00pm","(210) 233-7000"));
            }
            else if(position == 2){
                setAllData(holder, new LocationModel("Northwest Campus","8210 Callaghan San Antonio, Texas 78230",
                        "Monday to Friday","8:00am to 7:00pm","(210) 233-7000"));
            }
            else if(position == 3){
                setAllData(holder, new LocationModel("Potranco Campus","10002 Westover Bluff San Antonio, Texas 78251",
                        "Monday to Friday","8:00am to 7:00pm","(210) 521-9500"));
            }
            else if(position == 4){
                setAllData(holder, new LocationModel("Metropolitan Women’s Campus","1200 Brooklyn Ave, Suite 300 San Antonio, TX 78212",
                        "Monday to Friday","8:00am to 5:00pm","(210) 233-7230"));
            }
            else if(position == 5){
                setAllData(holder, new LocationModel("Hill Country Village Campus","14811 San Pedro Avenue San Antonio, TX 78232",
                        "Monday to Friday","8:00am to 7:00pm","(210) 494-7758"));
            }
            else if(position == 6){
                setAllData(holder, new LocationModel("Shavano Park Campus","3619 Paesanos Pkwy, Suite 212 San Antonio, Texas 78231",
                        "Monday to Friday","8:00am to 5:00pm","(210) 233-7126"));
            }
            else if(position == 7){
                setAllData(holder, new LocationModel("Las Palmas Campus","803 Castroville Road, Suite 416 San Antonio, Texas 78237",
                        "Mon,Tue,Wed/Thu/Fri","8am-6pm/9am-7pm/8am-12pm","(210) 233-7150"));
            }
        }
        else if(type.equals("2")){
            if(position == 0){
                setAllData(holder, new LocationModel("San Marcos Campus","1340 Wonder World Drive Suite 4201 San Marcos, Texas 78666",
                        "Monday to Friday","8:00am to 7:00pm","(512) 268-8900"));
            }
        }
        else if(type.equals("3")){
            if(position == 0){
                setAllData(holder, new LocationModel("Wimberley Campus","South River Center (next to Wimberley City Hall) 201 Stillwater Wimberley, Texas 78676",
                        "Monday to Friday","8:00am to 5:00pm","(512) 268-8930"));
            }
        }
        else if(type.equals("4")){
            if(position == 0){
                setAllData(holder, new LocationModel("Kyle Campus","2810 Dacy Lane Kyle, Texas 78640",
                        "Monday to Friday","8:00am to 7:00pm","(512) 268-8900"));
            }
        }
        else if(type.equals("5")){
            if(position == 0){
                setAllData(holder, new LocationModel("Boerne Campus","430 W Bandera Rd, (next to H.E.B.) Boerne, TX 78006",
                        "Monday to Friday","8:00am to 7:00pm","(830) 249-1717"));
            }
        }

        return convertView;
    }

    private void setAllData(ViewHolder holder, final LocationModel locationModel) {
        holder.title.setText(locationModel.getTitle());
        holder.address.setText(locationModel.getAddress());
        holder.timingTxt.setText(locationModel.getTimingTxt());
        holder.timing.setText(locationModel.getTiming());
        holder.phone.setText(locationModel.getPhone());
        holder.phonePanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+locationModel.getPhone()));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                context.startActivity(callIntent);
            }
        });

    }

    public static class ViewHolder {
        public Button viewServices;
        public TextView title, address, timingTxt, timing, phone;
        public ViewGroup phonePanel;
    }

}

