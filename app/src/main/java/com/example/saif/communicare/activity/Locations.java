package com.example.saif.communicare.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.util.Constants;

import java.util.ArrayList;

public class Locations extends BaseActivity {
    ViewGroup tab1, tab2, tab3, tab4, tab5;
    ViewGroup servicePanel, locationPanel;
    TextView service, location;
    Button search;
    ArrayList<String> locations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_locations, R.color.blue3, "Locations", true);

        tab1=findViewById(R.id.tab1);
        tab2=findViewById(R.id.tab2);
        tab3=findViewById(R.id.tab3);
        tab4=findViewById(R.id.tab4);
        tab5=findViewById(R.id.tab5);
        servicePanel=findViewById(R.id.servicePanle);
        locationPanel=findViewById(R.id.locationPanel);
        service=findViewById(R.id.service);
        location=findViewById(R.id.location);
        search=findViewById(R.id.search);
        servicePanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Locations.this);
                builder.setTitle("Select a Service");
                final ArrayList<String> services = Constants.getServices();
                builder.setSingleChoiceItems((String [])services.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        service.setText(services.get(which));
                        location.setText("Select a Location");
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        locationPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serviceTxt = service.getText().toString();
                if(serviceTxt.equalsIgnoreCase("Select a Service")){
                    return;
                }
                locations.clear();
                if(serviceTxt.equals(Constants.Behavioral)){
                    locations = Constants.getBehavioral();
                }
                else if(serviceTxt.equals(Constants.Dental)){
                    locations = Constants.getDental();
                }
                else if(serviceTxt.equals(Constants.Family)){
                    locations = Constants.getFamily();
                }
                else if(serviceTxt.equals(Constants.Hepatitis)){
                    locations = Constants.getHepatitis();
                }
                else if(serviceTxt.equals(Constants.Integrated)){
                    locations = Constants.getIntegrated();
                }
                else if(serviceTxt.equals(Constants.Metabolic)){
                    locations = Constants.getMetabolic();
                }
                else if(serviceTxt.equals(Constants.Pediatrics)){
                    locations = Constants.getPediatrics();
                }
                else if(serviceTxt.equals(Constants.Rheumatology)){
                    locations = Constants.getRheumatology();
                }
                else if(serviceTxt.equals(Constants.Senior)){
                    locations = Constants.getSenior();
                }
                else if(serviceTxt.equals(Constants.Vision)){
                    locations = Constants.getVision();
                }
                else if(serviceTxt.equals(Constants.Women)){
                    locations = Constants.getWomen();
                }
                else if(serviceTxt.equals(Constants.WIC)){
                    locations = Constants.getWIC();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(Locations.this);
                builder.setTitle("Select a Location");
                builder.setSingleChoiceItems((String [])locations.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        location.setText(locations.get(which));
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(location.getText().toString().equalsIgnoreCase("Select a Location")){
                    return;
                }

                String locationTxt = location.getText().toString();
                if(locationTxt.equals(Constants.Boerne)){
                    openCampusDetails("5", 0);
                }
                else if(locationTxt.equals(Constants.East)){
                    openCampusDetails("1", 1);
                }
                else if(locationTxt.equals(Constants.Hill)){
                    openCampusDetails("1", 5);
                }
                else if(locationTxt.equals(Constants.Kyle)){
                    openCampusDetails("4", 0);
                }
                else if(locationTxt.equals(Constants.Northwest)){
                    openCampusDetails("1", 2);
                }
                else if(locationTxt.equals(Constants.Potranco)){
                    openCampusDetails("1", 3);
                }
                else if(locationTxt.equals(Constants.San)){
                    openCampusDetails("2", 0);
                }
                else if(locationTxt.equals(Constants.West)){
                    openCampusDetails("1", 0);
                }
                else if(locationTxt.equals(Constants.Downtown)){
                    openCampusDetails("6", 0);
                }
                else if(locationTxt.equals(Constants.Wimberley)){
                    openCampusDetails("3", 0);
                }
                else if(locationTxt.equals(Constants.Shavano)){
                    openCampusDetails("1", 6);
                }
                else if(locationTxt.equals(Constants.Metropolitan)){
                    openCampusDetails("1", 4);
                }
                else if(locationTxt.equals(Constants.Las)){
                    openCampusDetails("1", 7);
                }
            }
        });

        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Locations.this, LocationDetails.class);
                intent.putExtra("type", "1");
                startActivity(intent);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Locations.this, LocationDetails.class);
                intent.putExtra("type", "2");
                startActivity(intent);
            }
        });
        tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Locations.this, LocationDetails.class);
                intent.putExtra("type", "3");
                startActivity(intent);
            }
        });
        tab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Locations.this, LocationDetails.class);
                intent.putExtra("type", "4");
                startActivity(intent);
            }
        });
        tab5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Locations.this, LocationDetails.class);
                intent.putExtra("type", "5");
                startActivity(intent);
            }
        });
    }

    public void openCampusDetails(String type, int position){
        Intent intent = new Intent(this, CampusDetails.class);
        intent.putExtra("type", type);
        intent.putExtra("position", position);
        startActivity(intent);
    }
}
