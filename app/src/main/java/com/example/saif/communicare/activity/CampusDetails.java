package com.example.saif.communicare.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.util.Constants;

import java.util.ArrayList;

public class CampusDetails extends BaseActivity {
    TextView title, title2, address, address2, timingTxt, timingTxt2, timing, timing2, phone, phone2, desc, dental, medical, behavioral;
    ImageView imageView;
    ViewGroup servicePanel, locationPanel, phonePanel1;
    TextView service, location;
    Button search;
    ArrayList<String> locations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String type = getIntent().getStringExtra("type");
        int position = getIntent().getIntExtra("position", 0);

        if(type.equals("1")){
            if(position == 0){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "West Campus", true);
                String desc_ = "•  Medical\n" +
                        "•  Family Medicine\n" +
                        "•  OB/GYN\n" +
                        "•  Pediatrics\n" +
                        "•  Senior Care\n" +
                        "•  Rheumatology\n" +
                        "•  Dental\n" +
                        "•  Behavioral Health\n" +
                        "•  Lab\n" +
                        "(Provided by Quest Diagnostics)\n" +
                        "•  Optometry & Optical Services\n" +
                        "(Provided by UIW Eye Institute)";
                setAllData(new LocationModel("West Campus","1102 Barclay Street San Antonio, Texas 78207",
                        "Monday to Friday","8:00am to 7:00pm","(210) 233-7000",
                        desc_, R.drawable.san_antonio, new Integer[]{1,2,3}));
            }
            else if(position == 1){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "East Campus", true);
                String desc_ = "• Medical\n" +
                        "• Family Medicine\n" +
                        "• OB/GYN\n" +
                        "• Pediatrics\n" +
                        "• Senior Care\n" +
                        "• Dental\n" +
                        "• Lab\n" +
                        "(Provided by Quest Diagnostics)";
                setAllData(new LocationModel("East Campus","3066 East Commerce San Antonio, Texas 78220",
                        "Monday to Friday","8:00am to 7:00pm","(210) 233-7000",
                        desc_, R.drawable.loc2, new Integer[]{1,2}));
            }
            else if(position == 2){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Northwest Campus", true);
                String desc_ = "• Family Medicine\n" +
                        "• Pediatrics Care\n" +
                        "• Senior Care\n" +
                        "• Behavioral Health\n" +
                        "• OB/GYN\n" +
                        "(Provided by University Health System)\n" +
                        "• Lab\n" +
                        "(Provided by Quest Diagnostics)";
                setAllData(new LocationModel("Northwest Campus","8210 Callaghan San Antonio, Texas 78230",
                        "Monday to Friday","8:00am to 7:00pm","(210) 233-7000",
                        desc_, R.drawable.loc3, new Integer[]{1,3}));
            }
            else if(position == 3){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Potranco Campus", true);
                String desc_ = "• Medical\n" +
                        "• Pediatric Care\n" +
                        "• Women’s Health\n" +
                        "• WIC\n" +
                        "• Lab\n" +
                        "(Provided by Quest Diagnostics)";
                setAllData(new LocationModel("Potranco Campus","10002 Westover Bluff San Antonio, Texas 78251",
                        "Monday to Friday","8:00am to 7:00pm","(210) 521-9500",
                        desc_, R.drawable.loc4, new Integer[]{1}));
            }
            else if(position == 4){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Metropolitan Women’s Campus", true);
                String desc_ = "• General Women’s Care\n" +
                        "• Family Planning\n" +
                        "• Pre-Pregnancy Counseling\n" +
                        "• Free Pregnancy Testing\n" +
                        "• Sexually Transmitted Diseases Counseling and Treatment\n" +
                        "• Well-Woman Care (Annual Physical)\n" +
                        "• Gynecological Problems\n" +
                        "• Obstetric Care\n" +
                        "• Prenatal Orientation/Education\n" +
                        "• Prenatal Care\n" +
                        "• Obstetric Deliveries\n" +
                        "• Post-Partum Education";
                setAllData(new LocationModel("Metropolitan Women’s Campus","1200 Brooklyn Ave, Suite 300 San Antonio, TX 78212",
                        "Monday to Friday","8:00am to 5:00pm","(210) 233-7230",
                        desc_, R.drawable.loc5, new Integer[]{1}));
            }
            else if(position == 5){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Hill Country Village Campus", true);
                String desc_ = "• Acute Care\n" +
                        "• Well-Child Visits\n" +
                        "• Chronic Disease Management (i.e. asthma, diabetes, etc.)\n" +
                        "• Immunizations\n" +
                        "• Health Education\n" +
                        "• ADHD Care";
                setAllData(new LocationModel("Hill Country Village Campus","14811 San Pedro Avenue San Antonio, TX 78232",
                        "Monday to Friday","8:00am to 7:00pm","(210) 494-7758",
                        desc_, R.drawable.loc6, new Integer[]{1}));
            }
            else if(position == 6){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Shavano Park Campus", true);
                String desc_ = "• Hepatitis Care\n" +
                        "• Lab\n" +
                        "(Provided by Quest Diagnostics)";
                setAllData(new LocationModel("Shavano Park Campus","3619 Paesanos Pkwy, Suite 212 San Antonio, Texas 78231",
                        "Monday to Friday","8:00am to 5:00pm","(210) 233-7126",
                        desc_, R.drawable.loc7, new Integer[]{1}));
            }
            else if(position == 7){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Las Palmas Campus", true);
                String desc_ = "• Nutritional education and counseling\n" +
                        "• Breast-feeding support\n" +
                        "• A portion of the formula needed to meet your baby’s requirements\n" +
                        "• Help with special formula\n" +
                        "• Access and referrals to other helpful programs";
                setAllData(new LocationModel("Las Palmas Campus","803 Castroville Road, Suite 416 San Antonio, Texas 78237",
                        "Mon,Tue,Wed/Thu/Fri","8am-6pm/9am-7pm/8am-12pm","(210) 233-7150",
                        desc_, R.drawable.loc8, new Integer[]{}));
            }
        }
        else if(type.equals("2")){
            if(position == 0){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "San Marcos Campus", true);
                String desc_ = "• Medical\n" +
                        "• Family Medicine\n" +
                        "• Senior Care\n" +
                        "• Women’s Health\n" +
                        "• Pediatrics\n" +
                        "• Lab\n" +
                        "(Provided by Quest Diagnostics)";
                setAllData(new LocationModel("San Marcos Campus","1340 Wonder World Drive Suite 4201 San Marcos, Texas 78666",
                        "Monday to Friday","8:00am to 7:00pm","(512) 268-8900",
                        desc_, R.drawable.loc9, new Integer[]{1}));
            }
        }
        else if(type.equals("3")){
            if(position == 0){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Wimberley Campus", true);
                String desc_ = "• Acute Care\n" +
                        "• Well-Child Visits\n" +
                        "• Chronic Disease Management (i.e asthma, diabetes,etc.)\n" +
                        "• Immunizations\n" +
                        "• Health Education\n" +
                        "• ADHD Care";
                setAllData(new LocationModel("Wimberley Campus","South River Center (next to Wimberley City Hall) 201 Stillwater Wimberley, Texas 78676",
                        "Monday to Friday","8:00am to 5:00pm","(512) 268-8930",
                        desc_, R.drawable.loc10, new Integer[]{1}));
            }
        }
        else if(type.equals("4")){
            if(position == 0){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Kyle Campus", true);
                String desc_ = "• Medical\n" +
                        "• Family Medicine\n" +
                        "• Senior Care\n" +
                        "• Women’s Health\n" +
                        "• Pediatrics\n" +
                        "• Dental\n" +
                        "• Behavioral Health\n" +
                        "• Lab\n" +
                        "(Provided by Quest Diagnostics)";
                setAllData(new LocationModel("Kyle Campus","2810 Dacy Lane Kyle, Texas 78640",
                        "Monday to Friday","8:00am to 7:00pm","(512) 268-8900",
                        desc_, R.drawable.loc11, new Integer[]{1,2,3}));
            }
        }
        else if(type.equals("5")){
            if(position == 0){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Boerne Campus", true);
                String desc_ = "• Pediatric Care\n" +
                        "• Pediatric Specialists\n" +
                        "(Provided by University Health System)\n" +
                        "• Neurology\n" +
                        "• Gastroenterology\n" +
                        "• Pulmonology\n" +
                        "• Nephrology\n" +
                        "• Allergy";
                setAllData(new LocationModel("Boerne Campus","430 W Bandera Rd, (next to H.E.B.) Boerne, TX 78006",
                        "Monday to Friday","8:00am to 7:00pm","(830) 249-1717",
                        desc_, R.drawable.loc12, new Integer[]{1}));
            }
        }
        else if(type.equals("6")){
            if(position == 0){
                replaceContentLayout(R.layout.activity_west_campus, R.color.orange4, "Downtown Campus", true);
                String desc_ = "• Medical\n" +
                        "• Family Medicine\n" +
                        "• Pediatrics\n" +
                        "• Senior Care\n" +
                        "• Lab\n" +
                        "(Provided by Quest Diagnostics)";
                setAllData(new LocationModel("Downtown Campus","Madison Square Medical Building 311 Camden, Suite 601 San Antonio, Texas 78215",
                        "Monday to Friday","9:00am to 8:00pm","(210) 233-7000",
                        desc_, R.drawable.loc13, new Integer[]{1}));
            }
        }

        servicePanel=findViewById(R.id.servicePanle);
        locationPanel=findViewById(R.id.locationPanel);
        service=findViewById(R.id.service);
        location=findViewById(R.id.location);
        search=findViewById(R.id.search);
        servicePanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CampusDetails.this);
                builder.setTitle("Select a Service");
                final ArrayList<String> services = Constants.getServices();
                builder.setSingleChoiceItems((String [])services.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        service.setText(services.get(which));
                        location.setText("Select a Location");
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        locationPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serviceTxt = service.getText().toString();
                if(serviceTxt.equalsIgnoreCase("Select a Service")){
                    return;
                }
                locations.clear();
                if(serviceTxt.equals(Constants.Behavioral)){
                    locations = Constants.getBehavioral();
                }
                else if(serviceTxt.equals(Constants.Dental)){
                    locations = Constants.getDental();
                }
                else if(serviceTxt.equals(Constants.Family)){
                    locations = Constants.getFamily();
                }
                else if(serviceTxt.equals(Constants.Hepatitis)){
                    locations = Constants.getHepatitis();
                }
                else if(serviceTxt.equals(Constants.Integrated)){
                    locations = Constants.getIntegrated();
                }
                else if(serviceTxt.equals(Constants.Metabolic)){
                    locations = Constants.getMetabolic();
                }
                else if(serviceTxt.equals(Constants.Pediatrics)){
                    locations = Constants.getPediatrics();
                }
                else if(serviceTxt.equals(Constants.Rheumatology)){
                    locations = Constants.getRheumatology();
                }
                else if(serviceTxt.equals(Constants.Senior)){
                    locations = Constants.getSenior();
                }
                else if(serviceTxt.equals(Constants.Vision)){
                    locations = Constants.getVision();
                }
                else if(serviceTxt.equals(Constants.Women)){
                    locations = Constants.getWomen();
                }
                else if(serviceTxt.equals(Constants.WIC)){
                    locations = Constants.getWIC();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(CampusDetails.this);
                builder.setTitle("Select a Location");
                builder.setSingleChoiceItems((String [])locations.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        location.setText(locations.get(which));
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(location.getText().toString().equalsIgnoreCase("Select a Location")){
                    return;
                }

                String locationTxt = location.getText().toString();
                if(locationTxt.equals(Constants.Boerne)){
                    openCampusDetails("5", 0);
                }
                else if(locationTxt.equals(Constants.East)){
                    openCampusDetails("1", 1);
                }
                else if(locationTxt.equals(Constants.Hill)){
                    openCampusDetails("1", 5);
                }
                else if(locationTxt.equals(Constants.Kyle)){
                    openCampusDetails("4", 0);
                }
                else if(locationTxt.equals(Constants.Northwest)){
                    openCampusDetails("1", 2);
                }
                else if(locationTxt.equals(Constants.Potranco)){
                    openCampusDetails("1", 3);
                }
                else if(locationTxt.equals(Constants.San)){
                    openCampusDetails("2", 0);
                }
                else if(locationTxt.equals(Constants.West)){
                    openCampusDetails("1", 0);
                }
                else if(locationTxt.equals(Constants.Downtown)){
                    openCampusDetails("6", 0);
                }
                else if(locationTxt.equals(Constants.Wimberley)){
                    openCampusDetails("3", 0);
                }
                else if(locationTxt.equals(Constants.Shavano)){
                    openCampusDetails("1", 6);
                }
                else if(locationTxt.equals(Constants.Metropolitan)){
                    openCampusDetails("1", 4);
                }
                else if(locationTxt.equals(Constants.Las)){
                    openCampusDetails("1", 7);
                }
            }
        });
    }

    private void setAllData(final LocationModel locationModel) {
        title = findViewById(R.id.title);
        address = findViewById(R.id.address);
        address2 = findViewById(R.id.address2);
        timingTxt = findViewById(R.id.timingTxt);
        timingTxt2 = findViewById(R.id.timingTxt2);
        timing = findViewById(R.id.timing);
        timing2 = findViewById(R.id.timing2);
        phone = findViewById(R.id.phone);
        phone2 = findViewById(R.id.phone2);
        desc = findViewById(R.id.desc);
        imageView = findViewById(R.id.image);
        dental = findViewById(R.id.dental);
        medical = findViewById(R.id.medical);
        behavioral = findViewById(R.id.behavioral);
        phonePanel1=findViewById(R.id.phonePanel1);

        title.setText(locationModel.getTitle());
        address.setText(locationModel.getAddress());
        timingTxt.setText(locationModel.getTimingTxt());
        timing.setText(locationModel.getTiming());
        phone.setText(locationModel.getPhone());
        phonePanel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+locationModel.getPhone()));
                if (ActivityCompat.checkSelfPermission(CampusDetails.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        address2.setText(locationModel.getAddress());
        timingTxt2.setText(locationModel.getTimingTxt());
        timing2.setText(locationModel.getTiming());
        phone2.setText(locationModel.getPhone());
        phone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+locationModel.getPhone()));
                if (ActivityCompat.checkSelfPermission(CampusDetails.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        imageView.setImageResource(locationModel.getImage());
        desc.setText(locationModel.getDesc());

        Integer [] providers = locationModel.getProviders();
        for (int i = 0; i < providers.length; i++) {
            if(providers[i] == 1){
                medical.setVisibility(View.VISIBLE);
            }
            else if(providers[i] == 2){
                dental.setVisibility(View.VISIBLE);
            }
            else if(providers[i] == 3){
                behavioral.setVisibility(View.VISIBLE);
            }
        }
    }

    public void openCampusDetails(String type, int position){
        Intent intent = new Intent(this, CampusDetails.class);
        intent.putExtra("type", type);
        intent.putExtra("position", position);
        startActivity(intent);
    }

}
