package com.example.saif.communicare.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.UpcomingAppAdapter;
import com.example.saif.communicare.adapter.VideoAdapter;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.VideoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Videos extends BaseActivity {
    ListView listView;
    VideoAdapter videoAdapter;
    ArrayList<VideoModel> models = new ArrayList<>();
    String apiKey = "AIzaSyCA-Vw7ZfD_WB1YXZMnnIG4rYyYY2mQrsA";
    String channelId = "UCXFfSFL2y0kge4O7cAwTD9A";
    String nextPageToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        replaceContentLayout(R.layout.activity_videos, R.color.yellow1, "Videos", true);

        listView=findViewById(R.id.listView);
        String url = "https://www.googleapis.com/youtube/v3/search?channelId="+channelId+"&part=snippet&type=video&maxResults=50&key="+apiKey;
        loadVideos(url);
    }

    public void loadVideos(final String url){
        showProgress();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("tag", response.toString());
                        hideProgress();
                        try {
                            JSONArray items = response.getJSONArray("items");
                            for(int i=0; i<items.length(); i++){
                                JSONObject item = items.getJSONObject(i);
                                String videoId = item.getJSONObject("id").getString("videoId");
                                JSONObject snippet = item.getJSONObject("snippet");
                                String date = snippet.getString("publishedAt");
                                String title = snippet.getString("title");
                                String desc = snippet.getString("description");
                                String thumb = snippet.getJSONObject("thumbnails").getJSONObject("default").getString("url");
                                String channel = snippet.getString("channelTitle");
                                Log.e("tag", "title: "+title);
                                Log.e("tag", "date: "+date);
                                Log.e("tag", "thumb: "+thumb);
                                Log.e("tag", "channel: "+channel);
                                Log.e("tag", "url: "+videoId);
                                VideoModel videoModel = new VideoModel();
                                videoModel.setTitle(title);
                                videoModel.setChannel(channel);
                                videoModel.setThumb(thumb);
                                String temp = date.substring(0,19).replace("T", " ");
                                videoModel.setDate(temp);
                                videoModel.setUrl("https://www.youtube.com/watch?v="+videoId);
                                models.add(videoModel);
                            }

                            if(response.has("nextPageToken")) {
                                nextPageToken = response.getString("nextPageToken");
                                if (nextPageToken != null) {
                                    String url = "https://www.googleapis.com/youtube/v3/search?channelId="+channelId+"&part=snippet&type=video&pageToken=" + nextPageToken + "&maxResults=50&key=" + apiKey;
                                    loadVideos(url);
                                }
                            }

                            if(videoAdapter == null) {
                                videoAdapter = new VideoAdapter(Videos.this, R.layout.videos_item, models);
                                listView.setAdapter(videoAdapter);
                            }
                            else{
                                videoAdapter.setData(models);
                            }
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(models.get(position).getUrl()));
                                    startActivity(i);
                                }
                            });
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Log.e("tag", "error: "+error.getMessage());
                        error.printStackTrace();
                        hideProgress();
                    }
                }
        );
        requestQueue.add(jsonObjectRequest);
    }

    public static String getTimeFromSecond (long seconds) {
        if (seconds < 60) {
            return seconds + "s ago";
        } else if (seconds < 3600) {
            return "" + (seconds / 60) + "m ago";
        } else if (seconds < 86400) {
            return "" + (seconds / 3600) + "h ago";
        } else if (seconds < 604800) {
            return "" + (seconds / 86400) + "d ago";
        } else if (seconds < 2419200){
            return "" + (seconds / 604800) + "w ago";
        } else if(seconds < 29030400){
            return "" + (seconds / 2419200) + "M ago";
        } else {
            return "" + (seconds / 29030400) + "Y ago";
        }
    }
}
