package com.example.saif.communicare.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.LocationsAdapter;
import com.example.saif.communicare.adapter.Refill2Adapter;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.model.Refill2Model;
import com.example.saif.communicare.view.ExpandableHeightGridView;

import java.util.ArrayList;

public class Refill2 extends BaseActivity {
    ExpandableHeightGridView listView;
    Refill2Adapter refill2Adapter;
    ArrayList<Refill2Model> models = new ArrayList<>();
    ViewGroup scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_refill2, R.color.purple1, "Refill Medications", true);

        scrollView=findViewById(R.id.scrollView);
        listView=findViewById(R.id.listView);
        fillDummy();
        refill2Adapter = new Refill2Adapter(this, R.layout.refill2_item, models);
        listView.setAdapter(refill2Adapter);
        listView.setExpanded(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,0);
            }
        }, 10);
    }

    private void fillDummy() {
        models.add(new Refill2Model());
        models.add(new Refill2Model());
        models.add(new Refill2Model());
        models.add(new Refill2Model());
    }

    public void step3(View view) {
        startActivity(new Intent(this, Refill3.class));
    }
}
