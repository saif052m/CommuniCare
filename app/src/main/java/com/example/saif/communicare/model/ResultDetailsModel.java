package com.example.saif.communicare.model;

/**
 * Created by saif on 3/28/2018.
 */

public class ResultDetailsModel {
    private boolean isExpanded;

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
