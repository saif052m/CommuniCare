package com.example.saif.communicare.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.example.saif.communicare.R;
import com.example.saif.communicare.util.Constants;

import java.util.ArrayList;

public class More extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_more, R.color.purple2, "More", true);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 351);
        }
    }

    public void onlineSupport(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.onlineSupport));
        startActivity(intent);
    }

    public void share(View view) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "CommuniCare");
        i.putExtra(Intent.EXTRA_TEXT, Constants.appLink);
        startActivity(Intent.createChooser(i, "Share App"));
    }

    public void rateOnStore(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.appLink));
        startActivity(intent);
    }

    public void openEastCampus(View view) {
        Intent intent = new Intent(this, CampusDetails.class);
        intent.putExtra("type", "1");
        intent.putExtra("position", 1);
        startActivity(intent);
    }

    public void callForAppointment(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose a number");
        final ArrayList<String> numbers = Constants.getNumbers();

        final String [] phones = new String[numbers.size()];
        for (int i = 0; i < numbers.size(); i++) {
            phones[i] = numbers.get(i);
        }
        builder.setSingleChoiceItems(phones, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String number = phones[which];
                String [] arr = number.split(": ");
                number = arr[1];
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:"+number));
                if (ActivityCompat.checkSelfPermission(More.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(intent);
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void openReminders(View view) {
        startActivity(new Intent(this, RemindersList.class));
    }
}
