package com.example.saif.communicare.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.RemindersAdapter;
import com.example.saif.communicare.model.ReminderModel;

import java.util.ArrayList;

public class RemindersList extends BaseActivity {
    ListView listView;
    RemindersAdapter remindersAdapter;
    ArrayList<ReminderModel> models = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_reminders_list, R.color.blue1, "Reminders", true);

        listView=findViewById(R.id.listView);
        fillDummy();
        remindersAdapter = new RemindersAdapter(this, R.layout.reminders_item, models);
        listView.setAdapter(remindersAdapter);
    }

    private void fillDummy() {
        models.add(new ReminderModel());
        models.add(new ReminderModel());
        models.add(new ReminderModel());
        models.add(new ReminderModel());
        models.add(new ReminderModel());
    }
}
