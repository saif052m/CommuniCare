package com.example.saif.communicare.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.activity.Refill;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.MedicationModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class MedicationsAdapter extends ArrayAdapter<MedicationModel> {
    private ArrayList<MedicationModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public MedicationsAdapter(Context context, int resource, ArrayList<MedicationModel> models) {
        super(context, resource, models);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.medications_item, null);
            holder.refill = convertView.findViewById(R.id.refill);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.refill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, Refill.class));
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        public TextView refill;
        // public MyTextView title, desc;
    }

}

