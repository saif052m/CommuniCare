package com.example.saif.communicare.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.activity.CampusDetails;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.model.MedicationModel;
import com.example.saif.communicare.model.Refill2Model;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class Refill2Adapter extends ArrayAdapter<Refill2Model> {
    private ArrayList<Refill2Model> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public Refill2Adapter(Context context, int resource, ArrayList<Refill2Model> models) {
        super(context, resource, models);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.refill2_item, null);
            //holder.viewServices = convertView.findViewById(R.id.view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        return convertView;
    }

    public static class ViewHolder {
        //public Button viewServices;
    }

}

