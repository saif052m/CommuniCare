package com.example.saif.communicare.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.LocationsAdapter;
import com.example.saif.communicare.adapter.MedicationsAdapter;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.model.MedicationModel;
import com.example.saif.communicare.util.Constants;
import com.example.saif.communicare.view.ExpandableHeightGridView;
import com.example.saif.communicare.view.MyMapFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class LocationDetails extends BaseActivity {
    ExpandableHeightGridView listView;
    LocationsAdapter locationsAdapter;
    ArrayList<LocationModel> models = new ArrayList<>();
    ViewGroup scrollView;
    SupportMapFragment mapFragment;
    ViewGroup servicePanel, locationPanel;
    TextView service, location;
    Button search;
    ArrayList<String> locations = new ArrayList<>();
    TextView showMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String type = getIntent().getStringExtra("type");
        if(type.equals("1"))
            replaceContentLayout(R.layout.activity_location_details, R.color.blue3, "San Antonio Locations", true);
        if(type.equals("2"))
            replaceContentLayout(R.layout.activity_location_details, R.color.blue3, "San Macros Locations", true);
        if(type.equals("3"))
            replaceContentLayout(R.layout.activity_location_details, R.color.blue3, "Wimberley Locations", true);
        if(type.equals("4"))
            replaceContentLayout(R.layout.activity_location_details, R.color.blue3, "Kyle Locations", true);
        if(type.equals("5"))
            replaceContentLayout(R.layout.activity_location_details, R.color.blue3, "Boerne Locations", true);

        showMap = findViewById(R.id.showMap);
        servicePanel=findViewById(R.id.servicePanle);
        locationPanel=findViewById(R.id.locationPanel);
        service=findViewById(R.id.service);
        location=findViewById(R.id.location);
        search=findViewById(R.id.search);
        servicePanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LocationDetails.this);
                builder.setTitle("Select a Service");
                final ArrayList<String> services = Constants.getServices();
                builder.setSingleChoiceItems((String [])services.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        service.setText(services.get(which));
                        location.setText("Select a Location");
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        locationPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serviceTxt = service.getText().toString();
                if(serviceTxt.equalsIgnoreCase("Select a Service")){
                    return;
                }
                locations.clear();
                if(serviceTxt.equals(Constants.Behavioral)){
                    locations = Constants.getBehavioral();
                }
                else if(serviceTxt.equals(Constants.Dental)){
                    locations = Constants.getDental();
                }
                else if(serviceTxt.equals(Constants.Family)){
                    locations = Constants.getFamily();
                }
                else if(serviceTxt.equals(Constants.Hepatitis)){
                    locations = Constants.getHepatitis();
                }
                else if(serviceTxt.equals(Constants.Integrated)){
                    locations = Constants.getIntegrated();
                }
                else if(serviceTxt.equals(Constants.Metabolic)){
                    locations = Constants.getMetabolic();
                }
                else if(serviceTxt.equals(Constants.Pediatrics)){
                    locations = Constants.getPediatrics();
                }
                else if(serviceTxt.equals(Constants.Rheumatology)){
                    locations = Constants.getRheumatology();
                }
                else if(serviceTxt.equals(Constants.Senior)){
                    locations = Constants.getSenior();
                }
                else if(serviceTxt.equals(Constants.Vision)){
                    locations = Constants.getVision();
                }
                else if(serviceTxt.equals(Constants.Women)){
                    locations = Constants.getWomen();
                }
                else if(serviceTxt.equals(Constants.WIC)){
                    locations = Constants.getWIC();
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(LocationDetails.this);
                builder.setTitle("Select a Location");
                builder.setSingleChoiceItems((String [])locations.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        location.setText(locations.get(which));
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(location.getText().toString().equalsIgnoreCase("Select a Location")){
                    return;
                }

                String locationTxt = location.getText().toString();
                if(locationTxt.equals(Constants.Boerne)){
                    openCampusDetails("5", 0);
                }
                else if(locationTxt.equals(Constants.East)){
                    openCampusDetails("1", 1);
                }
                else if(locationTxt.equals(Constants.Hill)){
                    openCampusDetails("1", 5);
                }
                else if(locationTxt.equals(Constants.Kyle)){
                    openCampusDetails("4", 0);
                }
                else if(locationTxt.equals(Constants.Northwest)){
                    openCampusDetails("1", 2);
                }
                else if(locationTxt.equals(Constants.Potranco)){
                    openCampusDetails("1", 3);
                }
                else if(locationTxt.equals(Constants.San)){
                    openCampusDetails("2", 0);
                }
                else if(locationTxt.equals(Constants.West)){
                    openCampusDetails("1", 0);
                }
                else if(locationTxt.equals(Constants.Downtown)){
                    openCampusDetails("6", 0);
                }
                else if(locationTxt.equals(Constants.Wimberley)){
                    openCampusDetails("3", 0);
                }
                else if(locationTxt.equals(Constants.Shavano)){
                    openCampusDetails("1", 6);
                }
                else if(locationTxt.equals(Constants.Metropolitan)){
                    openCampusDetails("1", 4);
                }
                else if(locationTxt.equals(Constants.Las)){
                    openCampusDetails("1", 7);
                }
            }
        });

        mapFragment = (MyMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                //googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                final ScrollView mScrollView = (ScrollView) findViewById(R.id.scrollView); //parent scrollview in xml, give your scrollview id value
                ((MyMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                        .setListener(new MyMapFragment.OnTouchListener() {
                            @Override
                            public void onTouch() {
                                mScrollView.requestDisallowInterceptTouchEvent(true);
                            }
                        });

                if(type.equals("1")) {
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.4098592, -98.5443361)).title("West Campus"));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.4187376, -98.4426746)).title("East Campus"));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.513638, -98.5514159)).title("Northwest Campus"));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.4370375, -98.6890226)).title("Potranco Campus"));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.4401684, -98.492695)).title("Metropolitan Women’s Campus"));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.5771534, -98.4810549)).title("Hill Country Campus"));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.5995918, -98.5563507)).title("Shavano Park Campus"));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.4177208, -98.5557254)).title("Las Palmas Campus"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(29.4177208, -98.5557254), 10.0f));
                }
                else if(type.equals("2")){
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.8498555, -97.9479822)).title("San Marcos Campus"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(29.8498555, -97.9479822), 10.0f));
                }
                else if(type.equals("3")){
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.9901934, -98.0925254)).title("Wimberley Campus"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(29.9901934, -98.0925254), 10.0f));
                }
                else if(type.equals("4")){
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(30.0147007, -97.8421288)).title("Kyle Campus"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.0147007, -97.8421288), 10.0f));
                }
                else if(type.equals("5")){
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(29.7812325, -98.7369109)).title("Boerne Campus"));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(29.7812325, -98.7369109), 10.0f));
                }
            }
        });


        scrollView=findViewById(R.id.scrollView);
        listView=findViewById(R.id.listView);
        if(type.equals("1")){
            fillDummy(8);
        }
        else if(type.equals("2")){
            fillDummy(1);
        }
        else if(type.equals("3")){
            fillDummy(1);
        }
        else if(type.equals("4")){
            fillDummy(1);
        }
        else if(type.equals("5")){
            fillDummy(1);
        }
        locationsAdapter = new LocationsAdapter(this, R.layout.locations_item, models, type);
        listView.setAdapter(locationsAdapter);
        listView.setExpanded(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,0);
            }
        }, 10);
    }

    public void openCampusDetails(String type, int position){
        Intent intent = new Intent(this, CampusDetails.class);
        intent.putExtra("type", type);
        intent.putExtra("position", position);
        startActivity(intent);
    }

    private void fillDummy(int count) {
        for (int i = 1; i <= count; i++) {
            models.add(new LocationModel());
        }
    }

    public void hideMap(View view) {
        ViewGroup mapPanel = findViewById(R.id.mapPanel);
        mapPanel.setVisibility(View.GONE);
        showMap.setVisibility(View.VISIBLE);
    }

    public void showMap(View view) {
        ViewGroup mapPanel = findViewById(R.id.mapPanel);
        mapPanel.setVisibility(View.VISIBLE);
        showMap.setVisibility(View.GONE);
    }
}
