package com.example.saif.communicare.activity;

import android.os.Bundle;

import com.example.saif.communicare.R;

public class ServiceDetails extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String type = getIntent().getStringExtra("type");

        if(type.equals("1"))
            replaceContentLayout(R.layout.activity_family_medicine, R.color.orange4, "Family Medicine", true);
        if(type.equals("2"))
            replaceContentLayout(R.layout.activity_dental, R.color.purple1, "Dental", true);
        if(type.equals("3"))
            replaceContentLayout(R.layout.activity_pediatrics, R.color.red1, "Pediatrics", true);
        if(type.equals("4"))
            replaceContentLayout(R.layout.activity_women_health, R.color.blue3, "Women’s Health", true);
        if(type.equals("5"))
            replaceContentLayout(R.layout.activity_senior_care, R.color.blue4, "Senior Care", true);
        if(type.equals("6"))
            replaceContentLayout(R.layout.activity_behave_health, R.color.yellow1, "Behavioral Health", true);
        if(type.equals("7"))
            replaceContentLayout(R.layout.activity_wic_program, R.color.purple2, "WIC Program", true);
        if(type.equals("8"))
            replaceContentLayout(R.layout.activity_vision_care, R.color.orange5, "Vision Care", true);

    }
}
