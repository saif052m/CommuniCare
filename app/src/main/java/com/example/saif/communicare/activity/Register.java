package com.example.saif.communicare.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.saif.communicare.R;

public class Register extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_register, R.color.white, "Register", true);
    }
}
