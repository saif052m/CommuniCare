package com.example.saif.communicare.model;

import java.util.ArrayList;

/**
 * Created by saif on 3/26/2018.
 */

public class LocationModel {
    String title, address, timingTxt, timing, phone, desc;
    int image;
    Integer [] providers;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public LocationModel(){

    }

    public Integer[] getProviders() {
        return providers;
    }

    public void setProviders(Integer [] providers) {
        this.providers = providers;
    }

    public LocationModel(String title, String address, String timingTxt, String timing, String phone) {
        this.title = title;
        this.address = address;
        this.timingTxt = timingTxt;
        this.timing = timing;
        this.phone = phone;
    }

    public LocationModel(String title, String address, String timingTxt, String timing, String phone, String desc, int image, Integer [] providers) {
        this.title = title;
        this.address = address;
        this.timingTxt = timingTxt;
        this.timing = timing;
        this.phone = phone;
        this.desc = desc;
        this.image = image;
        this.providers = providers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTimingTxt() {
        return timingTxt;
    }

    public void setTimingTxt(String timingTxt) {
        this.timingTxt = timingTxt;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
