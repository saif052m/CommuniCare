package com.example.saif.communicare.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.activity.CampusDetails;
import com.example.saif.communicare.activity.ResultDetails;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.model.MedicationModel;
import com.example.saif.communicare.model.ResultModel;
import com.example.saif.communicare.model.VideoModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class ResultsAdapter extends ArrayAdapter<ResultModel> {
    private ArrayList<ResultModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public ResultsAdapter(Context context, int resource, ArrayList<ResultModel> models) {
        super(context, resource, models);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.results_item, null);
            holder.viewResults = convertView.findViewById(R.id.viewResults);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.viewResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ResultDetails.class));
            }
        });

        return convertView;
    }

    public static class ViewHolder {
        //public Button viewServices;
        public TextView viewResults;
    }

}

