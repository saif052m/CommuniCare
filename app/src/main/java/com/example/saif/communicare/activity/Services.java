package com.example.saif.communicare.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;

import com.example.saif.communicare.R;

public class Services extends BaseActivity {
    ViewGroup tab1, tab2, tab3, tab4, tab5, tab6, tab7, tab8;
    ViewGroup row1, row2, row3, row4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_services, R.color.red1, "Services", true);

        row1 = findViewById(R.id.row1);
        row2 = findViewById(R.id.row2);
        row3 = findViewById(R.id.row3);
        row4 = findViewById(R.id.row4);

        tab1 = findViewById(R.id.tab1);
        tab2 = findViewById(R.id.tab2);
        tab3 = findViewById(R.id.tab3);
        tab4 = findViewById(R.id.tab4);
        tab5 = findViewById(R.id.tab5);
        tab6 = findViewById(R.id.tab6);
        tab7 = findViewById(R.id.tab7);
        tab8 = findViewById(R.id.tab8);
        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "1");
                startActivity(intent);
            }
        });
        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "2");
                startActivity(intent);
            }
        });
        tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "3");
                startActivity(intent);
            }
        });
        tab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "4");
                startActivity(intent);
            }
        });
        tab5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "5");
                startActivity(intent);
            }
        });
        tab6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "6");
                startActivity(intent);
            }
        });
        tab7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "7");
                startActivity(intent);
            }
        });
        tab8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Services.this, ServiceDetails.class);
                intent.putExtra("type", "8");
                startActivity(intent);
            }
        });
    }
}
