package com.example.saif.communicare.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.activity.CampusDetails;
import com.example.saif.communicare.activity.ResultDetails;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.model.MedicationModel;
import com.example.saif.communicare.model.ResultDetailsModel;
import com.example.saif.communicare.model.ResultModel;
import com.example.saif.communicare.model.VideoModel;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class ResultDetailsAdapter extends ArrayAdapter<ResultDetailsModel> {
    private ArrayList<ResultDetailsModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public ResultDetailsAdapter(Context context, int resource, ArrayList<ResultDetailsModel> models) {
        super(context, resource, models);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.result_details_item, null);
        final ImageView expand = convertView.findViewById(R.id.expand);
        final TextView desc = convertView.findViewById(R.id.desc);

        if(models.get(position).isExpanded()){
            expand.setImageResource(R.drawable.accordin_minus);
            desc.setVisibility(View.VISIBLE);
        }
        else{
            expand.setImageResource(R.drawable.accordin_plus);
            desc.setVisibility(View.GONE);
        }

        expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!models.get(position).isExpanded()){
                    for(ResultDetailsModel m: models){
                        m.setExpanded(false);
                    }
                    models.get(position).setExpanded(true);
                }
                else{
                    models.get(position).setExpanded(false);
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public void setData(ArrayList<ResultDetailsModel> models){
        this.models = models;
    }

    public static class ViewHolder {
        public TextView desc;
        public ImageView expand;
    }

}

