package com.example.saif.communicare.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.ResultsAdapter;
import com.example.saif.communicare.adapter.VideoAdapter;
import com.example.saif.communicare.model.ResultModel;
import com.example.saif.communicare.model.VideoModel;

import java.util.ArrayList;

public class Results extends BaseActivity {
    ListView listView;
    ResultsAdapter resultsAdapter;
    ArrayList<ResultModel> models = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_results, R.color.orange5, "Results", true);

        listView=findViewById(R.id.listView);
        fillDummy();
        resultsAdapter = new ResultsAdapter(this, R.layout.results_item, models);
        listView.setAdapter(resultsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private void fillDummy() {
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
        models.add(new ResultModel());
    }
}
