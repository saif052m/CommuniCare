package com.example.saif.communicare.activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.UpcomingAppAdapter;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.view.ExpandableHeightGridView;

import java.util.ArrayList;

public class AppointmentDetails extends BaseActivity {
    ExpandableHeightGridView listView;
    UpcomingAppAdapter upcomingAppAdapter;
    ArrayList<AppointmentModel> models = new ArrayList<>();
    ViewGroup scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_appointment_details, R.color.orange4, "Appointment Details", true);

        scrollView=findViewById(R.id.scrollView);
        listView = findViewById(R.id.listView);
        fillDummy();
        upcomingAppAdapter = new UpcomingAppAdapter(this, R.layout.appointments_item, models);
        listView.setAdapter(upcomingAppAdapter);
        listView.setExpanded(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,0);
            }
        }, 10);
    }

    private void fillDummy() {
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
    }
}
