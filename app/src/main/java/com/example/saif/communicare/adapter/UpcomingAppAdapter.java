package com.example.saif.communicare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.saif.communicare.R;
import com.example.saif.communicare.model.AppointmentModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class UpcomingAppAdapter extends ArrayAdapter<AppointmentModel> {
    private ArrayList<AppointmentModel> models = new ArrayList<>();
    private LayoutInflater mInflater;

    public UpcomingAppAdapter(Context context, int resource, ArrayList<AppointmentModel> models) {
        super(context, resource, models);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.appointments_item, null);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        return convertView;
    }

    public static class ViewHolder {
       // public MyTextView title, desc;
    }

}

