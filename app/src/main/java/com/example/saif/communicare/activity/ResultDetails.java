package com.example.saif.communicare.activity;

import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.ResultDetailsAdapter;
import com.example.saif.communicare.adapter.UpcomingAppAdapter;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.ResultDetailsModel;
import com.example.saif.communicare.view.ExpandableHeightGridView;

import java.util.ArrayList;

public class ResultDetails extends BaseActivity {
    ExpandableHeightGridView listView;
    ResultDetailsAdapter resultDetailsAdapter;
    ArrayList<ResultDetailsModel> models = new ArrayList<>();
    ViewGroup scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_result_details, R.color.orange5, "View Results", true);

        scrollView=findViewById(R.id.scrollView);
        listView = findViewById(R.id.listView);
        fillDummy();
        resultDetailsAdapter = new ResultDetailsAdapter(this, R.layout.result_details_item, models);
        listView.setAdapter(resultDetailsAdapter);
        listView.setExpanded(true);
        //resultDetailsAdapter.setData(models);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,0);
            }
        }, 10);
    }

    private void fillDummy() {
        models.add(new ResultDetailsModel());
        models.add(new ResultDetailsModel());
        models.add(new ResultDetailsModel());
        models.add(new ResultDetailsModel());
        models.add(new ResultDetailsModel());
        models.add(new ResultDetailsModel());
    }
}
