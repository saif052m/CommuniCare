package com.example.saif.communicare.activity;

import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.saif.communicare.R;

public class BaseActivity extends AppCompatActivity {
    ProgressBar progressBar;
    ViewGroup container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        container = (ViewGroup)findViewById(R.id.container);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    protected void replaceContentLayout(int sourceId, int color, String title, boolean backArrow) {
        View contentLayout = findViewById(R.id.content_frame);
        ViewGroup parent = (ViewGroup) contentLayout.getParent();
        int index = parent.indexOfChild(contentLayout);
        parent.removeView(contentLayout);
        contentLayout = getLayoutInflater().inflate(sourceId, parent, false);
        parent.addView(contentLayout, index);
        // action bar
        setActionBar(color, title, backArrow);
    }

    private void setActionBar(int color, String title, boolean backArrow){
        ActionBar abar = getSupportActionBar();
        if(abar != null) {
            abar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(color)));
            View viewActionBar = getLayoutInflater().inflate(R.layout.customer_actionbar, null);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                    ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.MATCH_PARENT,
                    Gravity.CENTER);
            TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
            textviewTitle.setText(title);
            if(color == R.color.white) {
                textviewTitle.setTextColor(getResources().getColor(R.color.black));
            }
            else{
                final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
                upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                abar.setHomeAsUpIndicator(upArrow);
                if (Build.VERSION.SDK_INT >= 21) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    window.setStatusBarColor(getResources().getColor(color));
                }
            }
            abar.setCustomView(viewActionBar, params);
            abar.setDisplayShowCustomEnabled(true);
            abar.setDisplayShowTitleEnabled(false);
            if(backArrow)
                abar.setDisplayHomeAsUpEnabled(true);
            abar.setElevation(0);
        }
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.e("tag", "onConfigurationChanged");
        /*container = (ViewGroup)findViewById(R.id.container);
        if(container == null)
            return;

        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            //code for portrait mode
            container.getLayoutParams().width = width;
        } else {
            //code for landscape mode
            container.getLayoutParams().width = (int) (width / 1.4);
        }*/
    }
}

