package com.example.saif.communicare.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.saif.communicare.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class VideoPlay extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    String apiKey = "AIzaSyCA-Vw7ZfD_WB1YXZMnnIG4rYyYY2mQrsA";
    YouTubePlayerView playerView;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);

        playerView=findViewById(R.id.youtube_view);
        url = getIntent().getStringExtra("url");

        playerView.initialize(apiKey, this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean b) {
        Log.e("tag", "onInitializationSuccess");
        player.loadVideo(url);
        player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        Log.e("tag", "onInitializationFailure");
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, 11).show();
        } else {
            String errorMessage = "There was some error in initializing the video player.";
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 11) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(apiKey, this);
        }
    }

    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }
}
