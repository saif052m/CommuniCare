package com.example.saif.communicare.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.UpcomingAppAdapter;
import com.example.saif.communicare.model.AppointmentModel;

import java.util.ArrayList;

public class UpcomingAppointments extends BaseActivity {
    ListView listView;
    UpcomingAppAdapter upcomingAppAdapter;
    ArrayList<AppointmentModel> models = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_upcoming_appointments, R.color.orange4, "Upcoming Appointments", true);

        listView = findViewById(R.id.listView);
        fillDummy();
        upcomingAppAdapter = new UpcomingAppAdapter(this, R.layout.appointments_item, models);
        listView.setAdapter(upcomingAppAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(UpcomingAppointments.this, AppointmentDetails.class));
            }
        });
    }

    private void fillDummy() {
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
        models.add(new AppointmentModel());
    }
}
