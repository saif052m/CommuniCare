package com.example.saif.communicare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.saif.communicare.R;

public class Login extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_login, R.color.white, "Sign In", false);
    }

    public void forgotPassword(View view) {
        startActivity(new Intent(this, ForgotPassword.class));
    }

    public void loginUser(View view) {
        startActivity(new Intent(this, Home.class));
        finish();
    }

    public void guestLogin(View view) {
        startActivity(new Intent(this, Home.class));
        finish();
    }

    public void registerUser(View view) {
        startActivity(new Intent(this, Register.class));
    }
}
