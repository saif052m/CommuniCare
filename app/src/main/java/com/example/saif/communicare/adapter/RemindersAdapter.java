package com.example.saif.communicare.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.activity.Refill;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.MedicationModel;
import com.example.saif.communicare.model.ReminderModel;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class RemindersAdapter extends ArrayAdapter<ReminderModel> {
    private ArrayList<ReminderModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public RemindersAdapter(Context context, int resource, ArrayList<ReminderModel> models) {
        super(context, resource, models);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.reminders_item, null);
            //holder.refill = convertView.findViewById(R.id.refill);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        return convertView;
    }

    public static class ViewHolder {
        //public TextView refill;
        // public MyTextView title, desc;
    }

}

