package com.example.saif.communicare.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.saif.communicare.R;
import com.example.saif.communicare.adapter.MedicationsAdapter;
import com.example.saif.communicare.adapter.UpcomingAppAdapter;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.MedicationModel;

import java.util.ArrayList;

public class Medications extends BaseActivity {
    ListView listView;
    MedicationsAdapter medicationsAdapter;
    ArrayList<MedicationModel> models = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceContentLayout(R.layout.activity_medications, R.color.purple1, "Medications", true);

        listView = findViewById(R.id.listView);
        fillDummy();
        medicationsAdapter = new MedicationsAdapter(this, R.layout.appointments_item, models);
        listView.setAdapter(medicationsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    private void fillDummy() {
        models.add(new MedicationModel());
        models.add(new MedicationModel());
        models.add(new MedicationModel());
        models.add(new MedicationModel());
        models.add(new MedicationModel());
        models.add(new MedicationModel());
        models.add(new MedicationModel());
        models.add(new MedicationModel());
    }
}
