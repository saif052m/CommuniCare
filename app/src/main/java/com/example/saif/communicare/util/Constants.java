package com.example.saif.communicare.util;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Usman K on 6/23/2017.
 */

public class Constants {
    public static String TAG = "tag";

    // More screen
    public static final String onlineSupport = "https://www.communicaresa.org/contact/";
    public static final String appLink = "https://play.google.com/apps";
    public static String teleHealth = "https://communicaresa.vsee.me/u/clinic";

    // mobile numbers for call
    public static ArrayList<String> getNumbers() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add("Boerne: 830-916-1717");
        numbers.add("Metro: 210-225-5930");
        numbers.add("Others: 210-233-7000");
        return numbers;
    }

    // campus names
    public static String Boerne = "Boerne Campus";
    public static String East = "East Campus";
    public static String Hill = "Hill Country Village Campus";
    public static String Kyle = "Kyle Campus";
    public static String Northwest = "Northwest Campus";
    public static String Potranco = "Potranco Campus";
    public static String San = "San Marcos Campus";
    public static String West = "West Campus";
    public static String Downtown = "Downtown Campus";
    public static String Wimberley = "Wimberley Campus";
    public static String Shavano = "Shavano Park Campus";
    public static String Metropolitan = "Metropolitan Campus";
    public static String Las = "Las Palmas Campus";

    public static String Behavioral = "Behavioral health";
    public static String Dental = "Dental Care";
    public static String Family = "Family Medicine";
    public static String Hepatitis = "Hepatitis Care";
    public static String Integrated = "Integrated Pediatric Medical and Dental Care";
    public static String Metabolic = "Metabolic Care";
    public static String Pediatrics = "Pediatrics";
    public static String Rheumatology = "Rheumatology";
    public static String Senior = "Senior Care";
    public static String Vision = "Vision Care";
    public static String Women = "Women's Health";
    public static String WIC = "Women, Infants and Children (WIC)";

    public static ArrayList<String> getServices() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Behavioral);
        numbers.add(Dental);
        numbers.add(Family);
        numbers.add(Hepatitis);
        numbers.add(Integrated);
        numbers.add(Metabolic);
        numbers.add(Pediatrics);
        numbers.add(Rheumatology);
        numbers.add(Senior);
        numbers.add(Vision);
        numbers.add(Women);
        numbers.add(WIC);
        return numbers;
    }

    public static ArrayList<String> getBehavioral() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Boerne);
        numbers.add(East);
        numbers.add(Hill);
        numbers.add(Kyle);
        numbers.add(Northwest);
        numbers.add(Potranco);
        numbers.add(San);
        numbers.add(West);
        return numbers;
    }

    public static ArrayList<String> getDental() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(East);
        numbers.add(Kyle);
        numbers.add(West);
        return numbers;
    }

    public static ArrayList<String> getFamily() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Boerne);
        numbers.add(Downtown);
        numbers.add(East);
        numbers.add(Kyle);
        numbers.add(Northwest);
        numbers.add(San);
        numbers.add(Shavano);
        numbers.add(West);
        numbers.add(Wimberley);
        return numbers;
    }

    public static ArrayList<String> getHepatitis() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Northwest);
        return numbers;
    }

    public static ArrayList<String> getIntegrated() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(East);
        numbers.add(Kyle);
        numbers.add(West);
        return numbers;
    }

    public static ArrayList<String> getMetabolic() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Northwest);
        return numbers;
    }

    public static ArrayList<String> getPediatrics() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Boerne);
        numbers.add(Downtown);
        numbers.add(East);
        numbers.add(Hill);
        numbers.add(Kyle);
        numbers.add(Northwest);
        numbers.add(Potranco);
        numbers.add(San);
        numbers.add(Shavano);
        numbers.add(West);
        numbers.add(Wimberley);
        return numbers;
    }

    public static ArrayList<String> getRheumatology() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(West);
        return numbers;
    }

    public static ArrayList<String> getSenior() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Boerne);
        numbers.add(Downtown);
        numbers.add(East);
        numbers.add(Kyle);
        numbers.add(Northwest);
        numbers.add(San);
        numbers.add(Shavano);
        numbers.add(West);
        numbers.add(Wimberley);
        return numbers;
    }

    public static ArrayList<String> getVision() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(West);
        return numbers;
    }

    public static ArrayList<String> getWomen() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(East);
        numbers.add(Kyle);
        numbers.add(Metropolitan);
        numbers.add(Northwest);
        numbers.add(Potranco);
        numbers.add(San);
        numbers.add(West);
        return numbers;
    }

    public static ArrayList<String> getWIC() {
        ArrayList<String> numbers = new ArrayList<>();
        numbers.add(Las);
        numbers.add(Northwest);
        return numbers;
    }
}