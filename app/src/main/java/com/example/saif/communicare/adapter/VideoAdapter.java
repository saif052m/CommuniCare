package com.example.saif.communicare.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.saif.communicare.R;
import com.example.saif.communicare.activity.CampusDetails;
import com.example.saif.communicare.model.AppointmentModel;
import com.example.saif.communicare.model.LocationModel;
import com.example.saif.communicare.model.MedicationModel;
import com.example.saif.communicare.model.VideoModel;
import com.example.saif.communicare.network.LruBitmapCache;
import com.example.saif.communicare.util.CustomNetworkImageView;

import java.util.ArrayList;

/**
 * Created by saif on 2/17/2018.
 */

public class VideoAdapter extends ArrayAdapter<VideoModel> {
    private ArrayList<VideoModel> models = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    RequestQueue mRequestQueue;
    ImageLoader mImageLoader;

    public VideoAdapter(Context context, int resource, ArrayList<VideoModel> models) {
        super(context, resource, models);
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.models = models;
        this.context = context;
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.videos_item, null);
            holder.imageView=convertView.findViewById(R.id.imageView);
            holder.title=convertView.findViewById(R.id.title);
            holder.channel=convertView.findViewById(R.id.channel);
            holder.date=convertView.findViewById(R.id.date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        VideoModel model = models.get(position);

        holder.imageView.setDefaultImageResId(R.color.black);
        holder.imageView.setImageUrl(model.getThumb(), mImageLoader);
        holder.title.setText(model.getTitle());
        holder.channel.setText(model.getChannel());
        String txt = "Published: \n"+model.getDate();
        holder.date.setText(txt);

        return convertView;
    }

    public void setData(ArrayList<VideoModel> models) {
        this.models = models;
    }

    public static class ViewHolder {
        CustomNetworkImageView imageView;
        TextView title, channel, date;
    }

}

