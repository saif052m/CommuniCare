package com.example.saif.communicare.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by saif on 9/21/2016.
 */
public class MyPref {
    private static final String PREFS_NAME = "com.example.saif.communicare";

    public static void setId(Context context, String id){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString("user_id", id).apply();
    }
    public static String getId(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("user_id", null);
    }

    public static void setUsername(Context context, String username){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString("username", username).apply();
    }
    public static String getUsername(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("username", "");
    }
    public static void setLogin(Context context, int login) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putInt("isUserlogin", login).apply();
    }
    public static int getLogin(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getInt("isUserlogin", 0);
    }
    public static void setPhoto(Context context, String photo) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString("photo", photo).apply();
    }
    public static String getPhoto(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("photo", "");
    }

    public static void setEmail(Context context, String email) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString("email", email).apply();
    }
    public static String getEmail(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("email", "");
    }

    public static void setCount(Context context, int count) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putInt("notification_count", count).apply();
    }
    public static int getCount(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getInt("notification_count", 0);
    }

    public static void setCountry(Context context, String country) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        prefs.edit().putString("country", country).apply();
    }
    public static String getCountry(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getString("country", "");
    }

    public static void reset(Context context) {
        setLogin(context, 0);
        setUsername(context, null);
        setId(context, null);
        setEmail(context, null);
        setPhoto(context, null);
    }
}
